# Oasis

Oasis est un outil de cartographie de stations sismiques. Ainsi, il renseigne sur la position géographique de stations décrites au format [StationXML](https://www.fdsn.org/xml/station/). Celles-ci sont alors représentées sur une vue 3D de la Terre grâce à la bibliothèque [osgEarth](http://docs.osgearth.org/).

## Prérequis

Ce projet a été élaboré sur Windows 10. Le script *setenv.bat* suppose la disponibilité des éléments suivants :

- Visual Studio 2017 Professionnal ;
- Qt 5.12.12 (64 bits) ;
- CMake 3.14 (ou supérieur).

## Compilation

```bash
.\setenv.bat
.\vcpkg\bootstrap-vcpkg.bat -disableMetrics
.\vcpkg\vcpkg install osgearth:x64-windows
cd build
cmake -G "NMake Makefiles" -DCMAKE_BUILD_TYPE=Debug -DCMAKE_TOOLCHAIN_FILE=..\vcpkg\scripts\buildsystems\vcpkg.cmake ..
nmake
```

## Licence

[MIT](https://choosealicense.com/licenses/mit/)
