#include "stationxml_labels.h"

namespace oasis {

namespace stationxml {

// Éléments
const QString STATION_ELEMENT_NAME = QStringLiteral("Station");
const QString LATITUDE_ELEMENT_NAME = QStringLiteral("Latitude");
const QString LONGITUDE_ELEMENT_NAME = QStringLiteral("Longitude");
const QString ELEVATION_ELEMENT_NAME = QStringLiteral("Elevation");

// Attributs
const QString CODE_ATTRIBUTE_NAME = QStringLiteral("code");

} // namespace stationxml

} // namespace oasis
