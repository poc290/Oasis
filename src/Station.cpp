#include "Station.h"

namespace oasis {

Station::Station(const QString & code, const Location & location)
    : m_Code{code}, m_Location{location}
{
}

QString Station::getCode() const { return m_Code; }
Location Station::getLocation() const { return m_Location; }

} // namespace oasis
