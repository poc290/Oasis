#include "MapWidget.h"

#include <QtGui/QMouseEvent>

#include <osgEarth/EarthManipulator>
#include <osgEarth/MapNode>
#include <osgEarth/TMS>

#include "Camera.h"
#include "StationNode.h"

namespace {

// La scène créée représente le globe terrestre.
osg::Node * createScene()
{
    auto * const layer = new osgEarth::TMSImageLayer;
    layer->setURL("https://readymap.org/readymap/tiles/1.0.0/7/");

    // Un objet `MapNode` correspond au noeud racine du graphe de la scène.
    auto * const scene = new osgEarth::MapNode;
    scene->getMap()->addLayer(layer);

    return scene;
}

unsigned int getMouseButton(QMouseEvent * event)
{
    auto button = 0;
    switch (event->button()) {
        case Qt::LeftButton:
            button = 1;
            break;

        case Qt::MiddleButton:
            button = 2;
            break;

        case Qt::RightButton:
            button = 3;
            break;

        default:
            break;
    }

    return button;
}

} // namespace

namespace oasis {

MapWidget::MapWidget(QWidget * parent)
    : QOpenGLWidget{parent},
      m_GraphicsWindow{new osgViewer::GraphicsWindowEmbedded{x(), y(), width(), height()}},
      m_Viewer{new osgViewer::Viewer}
{
    m_Viewer->setCamera(new Camera{m_GraphicsWindow});
    m_Viewer->setSceneData(createScene());
    m_Viewer->setCameraManipulator(new osgEarth::EarthManipulator);
    m_Viewer->setThreadingModel(osgViewer::Viewer::SingleThreaded);
    m_Viewer->realize();

    setMinimumSize(500, 500);
}

void MapWidget::addStations(const QVector<QSharedPointer<Station>> & stations) const
{
    auto * const scene = dynamic_cast<osgEarth::MapNode *>(m_Viewer->getSceneData());
    if (scene) {
        auto * const group = new osg::Group;
        scene->addChild(group);

        const auto * const srs = scene->getMapSRS()->getGeographicSRS();

        for (const auto & station : stations) {
            group->addChild(new StationNode{station.get(), srs});
        }
    }
}

void MapWidget::paintGL()
{
    // https://docs.osgearth.org/en/latest/faq.html#rendering-problems-when-using-the-qt-qopenglwidget
    m_Viewer->getCamera()->getGraphicsContext()->setDefaultFboId(defaultFramebufferObject());

    m_Viewer->frame();
}

void MapWidget::resizeGL(int w, int h)
{
    m_GraphicsWindow->getEventQueue()->windowResize(x(), y(), w, h);
    m_GraphicsWindow->resized(x(), y(), w, h);

    auto * const camera = m_Viewer->getCamera();
    camera->setViewport(0, 0, width(), height());
}

void MapWidget::initializeGL() {}

bool MapWidget::event(QEvent * event)
{
    const auto handled = QOpenGLWidget::event(event);
    update();

    return handled;
}

void MapWidget::mousePressEvent(QMouseEvent * event)
{
    m_GraphicsWindow->getEventQueue()->mouseButtonPress(event->x(), event->y(),
                                                        getMouseButton(event));
}

void MapWidget::mouseReleaseEvent(QMouseEvent * event)
{
    m_GraphicsWindow->getEventQueue()->mouseButtonRelease(event->x(), event->y(),
                                                          getMouseButton(event));
}

void MapWidget::mouseMoveEvent(QMouseEvent * event)
{
    m_GraphicsWindow->getEventQueue()->mouseMotion(event->x(), event->y());
}

void MapWidget::wheelEvent(QWheelEvent * event)
{
    const auto motion = (event->angleDelta().y() > 0) ? osgGA::GUIEventAdapter::SCROLL_UP
                                                      : osgGA::GUIEventAdapter::SCROLL_DOWN;
    m_GraphicsWindow->getEventQueue()->mouseScroll(motion);
}

} // namespace oasis
