#ifndef OASIS_CAMERA_H
#define OASIS_CAMERA_H

#include <osg/Camera>

namespace osgViewer {

class GraphicsWindow;

} // namespace osgViewer

namespace oasis {

// Classe représentant une caméra pour transmettre une scène en 3D
class Camera : public osg::Camera {
public:
    explicit Camera(osgViewer::GraphicsWindow * window);
};

} // namespace oasis

#endif // OASIS_CAMERA_H
