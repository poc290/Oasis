#ifndef OASIS_MAPWIDGET_H
#define OASIS_MAPWIDGET_H

#include <QtWidgets/QOpenGLWidget>

#include <osg/ref_ptr>
#include <osgViewer/GraphicsWindow>
#include <osgViewer/Viewer>

namespace oasis {

class Station;

class MapWidget : public QOpenGLWidget {
public:
    explicit MapWidget(QWidget * parent = nullptr);

    void addStations(const QVector<QSharedPointer<Station>> & stations) const;

protected:
    void paintGL() override;
    void resizeGL(int w, int h) override;
    void initializeGL() override;

    // Cette méthode est ré-implémentée de telle sorte que le contenu de la fenêtre est mis à jour à
    // la suite de chaque action utilisateur.
    bool event(QEvent * event) override;

    // Ces méthodes sont redéfinies pour connecter les évènements souris gérés par Qt à ceux gérés
    // par osgEarth.
    void mousePressEvent(QMouseEvent * event) override;
    void mouseReleaseEvent(QMouseEvent * event) override;
    void mouseMoveEvent(QMouseEvent * event) override;
    void wheelEvent(QWheelEvent * event) override;

private:
    osg::ref_ptr<osgViewer::GraphicsWindowEmbedded> m_GraphicsWindow;
    osg::ref_ptr<osgViewer::Viewer> m_Viewer;
};

} // namespace oasis

#endif // OASIS_MAPWIDGET_H
