#ifndef OASIS_LOCATION_H
#define OASIS_LOCATION_H

namespace oasis {

class Location {
public:
    Location(double latitude, double longitude, double altitude);

    bool isValid() const;

    double getLatitude() const;
    double getLongitude() const;
    double getAltitude() const;

private:
    const double m_Latitude;  // Latitude exprimée en degré décimal
    const double m_Longitude; // Longitude exprimée en degré décimal
    const double m_Altitude;  // Altitude exprimée en mètre
};

} // namespace oasis

#endif // OASIS_LOCATION_H
