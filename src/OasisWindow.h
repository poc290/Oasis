#ifndef OASIS_OASISWINDOW_H
#define OASIS_OASISWINDOW_H

#include <QtWidgets/QWidget>

#include "StationXmlReader.h"

namespace oasis {

class OasisWindow : public QWidget {
public:
    explicit OasisWindow(QWidget * parent = nullptr);

private:
    StationXmlReader m_StationXmlReader;
};

} // namespace oasis

#endif // OASIS_OASISWINDOW_H
