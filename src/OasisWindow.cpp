#include "OasisWindow.h"

#include <QtWidgets/QGridLayout>

#include "MapWidget.h"
#include "StationXmlReader.h"

namespace {

const auto STATIONXML_FILE_PATH = QStringLiteral(":/data/stations.xml");
const auto STATION_COUNT = 170;

} // namespace

namespace oasis {

OasisWindow::OasisWindow(QWidget * parent) : QWidget{parent}
{
    // Chargement des stations à afficher
    const auto stations = []() {
        StationXmlReader reader;
        reader.read(STATIONXML_FILE_PATH);

        return reader.getStations();
    }();

    Q_ASSERT(stations.size() == STATION_COUNT);

    // Affichage du globe terrestre en 3D
    auto * const widget = new MapWidget;

    auto * const layout = new QGridLayout;
    layout->addWidget(widget, 0, 0);
    setLayout(layout);

    // Affichage des stations sismiques
    widget->addStations(stations);
}

} // namespace oasis
