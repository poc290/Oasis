#ifndef OASIS_STATIONXMLREADER_H
#define OASIS_STATIONXMLREADER_H

#include <QtCore/QXmlStreamReader>

namespace oasis {

class Station;

class StationXmlReader {
public:
    StationXmlReader() = default;

    void read(const QString & filePath);

    QVector<QSharedPointer<Station>> getStations() const;

private:
    QXmlStreamReader m_XmlParser;
    QVector<QSharedPointer<Station>> m_Stations;
};

} // namespace oasis

#endif // OASIS_STATIONXMLREADER_H
