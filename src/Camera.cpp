#include "Camera.h"

#include <QtGlobal>

#include <osgViewer/GraphicsWindow>

#include "WindowSize.h"

namespace {

//*******************************
// Paramètres du champ de vision
//*******************************
const auto FOVY = 30.0;   // Angle du champ dans le plan vertical (en degré)
const auto ZNEAR = 1.0;   // Distance entre la caméra et le plan proche
const auto ZFAR = 1000.0; // Distance entre la caméra et le plan loin

} // namespace

namespace oasis {

Camera::Camera(osgViewer::GraphicsWindow * window)
{
    // La caméra est associée à une fenêtre qui représente la scène.
    setGraphicsContext(window);

    // Dimensions de la fenêtre
    const auto windowSize = [=]() {
        auto x = 0;
        auto y = 0;
        auto width = 0;
        auto height = 0;
        window->getWindowRectangle(x, y, width, height);

        Q_UNUSED(x)
        Q_UNUSED(y)

        return WindowSize{width, height};
    }();

    // Définition de la zone de la fenêtre où est dessinée la scène
    setViewport(0, 0, windowSize.getWidth(), windowSize.getHeight());

    // Définition du champ de vision de la caméra, équivalent à une pyramide tronquée (*view
    // frustum* en anglais) : tous les objets situés hors de ce champ ne seront pas affichés.
    setProjectionMatrixAsPerspective(FOVY, windowSize.getAspectRatio(), ZNEAR, ZFAR);
}

} // namespace oasis
