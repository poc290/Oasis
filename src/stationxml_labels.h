#ifndef STATIONXML_LABELS_H
#define STATIONXML_LABELS_H

#include <QtCore/QString>

namespace oasis {

namespace stationxml {

// Éléments
extern const QString STATION_ELEMENT_NAME;
extern const QString LATITUDE_ELEMENT_NAME;
extern const QString LONGITUDE_ELEMENT_NAME;
extern const QString ELEVATION_ELEMENT_NAME;

// Attributs
extern const QString CODE_ATTRIBUTE_NAME;

} // namespace stationxml

} // namespace oasis

#endif // STATIONXML_LABELS_H
