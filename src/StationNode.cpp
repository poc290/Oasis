#include "StationNode.h"

#include "Station.h"

namespace {

const auto STATION_ICON_PATH = QString{OASIS_RESOURCES_PATH "/icons/station.png"}.toStdString();

} // namespace

namespace oasis {

StationNode::StationNode(Station * station, const osgEarth::SpatialReference * srs)
    : osgEarth::PlaceNode{osgEarth::GeoPoint{srs, station->getLocation().getLongitude(),
                                             station->getLocation().getLatitude()},
                          station->getCode().toStdString()}
{
    const auto style = []() {
        auto style = osgEarth::Style{};
        style.getOrCreate<osgEarth::IconSymbol>()->url()->setLiteral(STATION_ICON_PATH);
        style.getOrCreate<osgEarth::IconSymbol>()->declutter() = true;
        style.getOrCreate<osgEarth::IconSymbol>()->scale() = 0.1;
        style.getOrCreate<osgEarth::TextSymbol>()->halo() = osgEarth::Color{"#5f5f5f"};

        return style;
    }();

    setStyle(style);
}

} // namespace oasis
