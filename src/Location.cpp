#include "Location.h"

#include <cmath>

namespace oasis {

Location::Location(double latitude, double longitude, double altitude)
    : m_Latitude{latitude}, m_Longitude{longitude}, m_Altitude{altitude}
{
}

bool Location::isValid() const
{
    return !std::isnan(m_Latitude) && !std::isnan(m_Longitude) && !std::isnan(m_Altitude);
}

double Location::getLatitude() const { return m_Latitude; }
double Location::getLongitude() const { return m_Longitude; }
double Location::getAltitude() const { return m_Altitude; }

} // namespace oasis
