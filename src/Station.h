#ifndef OASIS_STATION_H
#define OASIS_STATION_H

#include <QtCore/QString>

#include "Location.h"

namespace oasis {

class Station {
public:
    Station(const QString & code, const Location & location);

    QString getCode() const;
    Location getLocation() const;

private:
    const QString m_Code;
    const Location m_Location;
};

} // namespace oasis

#endif // OASIS_STATION_H
