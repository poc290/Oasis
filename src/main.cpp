#include <iostream>

#include <QtGui/QIcon>
#include <QtWidgets/QApplication>

#include <osgEarth/Common>

#include "OasisWindow.h"

int main(int argc, char * argv[])
{
    QApplication app{argc, argv};
    app.setApplicationName(QStringLiteral("Oasis"));
    app.setWindowIcon(QIcon{QStringLiteral(":/icons/oasis.ico")});

    // La bibliothèque osgEarth doit être initialisée avant d'être utilisée.
    osgEarth::initialize();

    oasis::OasisWindow window;
    window.show();

    return app.exec();
}
