#ifndef OASIS_STATIONNODE_H
#define OASIS_STATIONNODE_H

#include <osgEarth/PlaceNode>

namespace oasis {

class Station;

class StationNode : public osgEarth::PlaceNode {
public:
    StationNode(Station * station, const osgEarth::SpatialReference * srs);
};

} // namespace oasis

#endif // OASIS_STATIONNODE_H
