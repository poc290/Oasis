#ifndef OASIS_WINDOWSIZE_H
#define OASIS_WINDOWSIZE_H

namespace oasis {

class WindowSize {
public:
    WindowSize(int width, int height);

    int getWidth() const;
    int getHeight() const;
    double getAspectRatio() const;

private:
    const int m_Width;
    const int m_Height;
    const double m_AspectRatio;
};

} // namespace oasis

#endif // OASIS_WINDOWSIZE_H
