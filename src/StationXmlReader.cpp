#include "StationXmlReader.h"

#include <cmath>

#include <QtCore/QDebug>
#include <QtCore/QFile>

#include "Station.h"
#include "stationxml_labels.h"

namespace {

oasis::Location readLocation(QXmlStreamReader & parser)
{
    using namespace oasis;
    using namespace stationxml;

    auto latitude = NAN;
    auto longitude = NAN;
    auto altitude = NAN;

    while (parser.readNextStartElement()) {
        if (parser.name() == LATITUDE_ELEMENT_NAME) {
            auto ok = false;
            const auto val = parser.readElementText().toDouble(&ok);
            if (ok) {
                latitude = val;
            }
        }
        else if (parser.name() == LONGITUDE_ELEMENT_NAME) {
            auto ok = false;
            const auto val = parser.readElementText().toDouble(&ok);
            if (ok) {
                longitude = val;
            }
        }
        else if (parser.name() == ELEVATION_ELEMENT_NAME) {
            auto ok = false;
            const auto val = parser.readElementText().toDouble(&ok);
            if (ok) {
                altitude = val;
            }
        }
        else {
            parser.skipCurrentElement();
        }
    }

    return Location{latitude, longitude, altitude};
}

QSharedPointer<oasis::Station> readStation(QXmlStreamReader & parser)
{
    using namespace oasis;
    using namespace stationxml;

    // Lecture du code de la station
    const auto code = [&]() {
        if (parser.attributes().hasAttribute(CODE_ATTRIBUTE_NAME)) {
            return parser.attributes().value(CODE_ATTRIBUTE_NAME).toString();
        }

        Q_UNREACHABLE();
    }();

    // Lecture de la position de la station
    const auto location = readLocation(parser);

    Q_ASSERT(location.isValid());

    return QSharedPointer<Station>::create(code, location);
}

} // namespace

namespace oasis {

void StationXmlReader::read(const QString & filePath)
{
    using namespace stationxml;

    QFile file{filePath};
    if (!file.open(QFile::ReadOnly | QFile::Text)) {
        qDebug() << "Impossible d'ouvrir le fichier" << filePath << ':' << file.errorString();
        return;
    }

    // Le fichier en question doit être ouvert préalablement à son traitement.
    m_XmlParser.setDevice(&file);

    // AFAIRE : vérifier la validité du document XML par rapport au schéma de référence

    while (!m_XmlParser.atEnd()) {
        if (m_XmlParser.readNext() == QXmlStreamReader::StartElement) {
            if (m_XmlParser.name() == STATION_ELEMENT_NAME) {
                m_Stations << readStation(m_XmlParser);
            }
        }
    }
}

QVector<QSharedPointer<Station>> StationXmlReader::getStations() const { return m_Stations; }

} // namespace oasis
