#include "WindowSize.h"

namespace oasis {

WindowSize::WindowSize(int width, int height)
    : m_Width{width}, m_Height{height}, m_AspectRatio{static_cast<double>(width) / height}
{
}

int WindowSize::getWidth() const { return m_Width; }
int WindowSize::getHeight() const { return m_Height; }
double WindowSize::getAspectRatio() const { return m_AspectRatio; }

} // namespace oasis
