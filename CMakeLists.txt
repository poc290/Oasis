cmake_minimum_required(VERSION 3.14)

project(Oasis)

list(APPEND CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake/modules")

include(cmake/OasisUtils.cmake)

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

find_package(Qt5 COMPONENTS Widgets REQUIRED)

find_package(OpenSceneGraph REQUIRED COMPONENTS osg osgDB osgGA osgUtil osgViewer)

oasis_copy_osg_plugins()

# La commande `find_package` ci-après ne permet pas d'utiliser la version DEBUG de la bibliothèque.
# find_package(osgEarth CONFIG REQUIRED)
find_package(osgEarth REQUIRED)

set(OASIS_SRC_FILES src/main.cpp
                    src/OasisWindow.cpp
                    src/OasisWindow.h
                    src/StationXmlReader.cpp
                    src/StationXmlReader.h
                    src/stationxml_labels.cpp
                    src/stationxml_labels.h
                    src/Station.cpp
                    src/Station.h
                    src/Location.cpp
                    src/Location.h
                    src/MapWidget.cpp
                    src/MapWidget.h
                    src/Camera.cpp
                    src/Camera.h
                    src/WindowSize.cpp
                    src/WindowSize.h
                    src/StationNode.cpp
                    src/StationNode.h)

add_executable(oasis ${OASIS_SRC_FILES}
                     resources/oasis.qrc)

target_include_directories(oasis PRIVATE ${OSGEARTH_INCLUDE_DIR})

target_link_libraries(oasis PRIVATE Qt5::Widgets
                                    ${OPENSCENEGRAPH_LIBRARIES}
                                    ${OSGEARTH_LIBRARY})

target_compile_definitions(oasis PUBLIC OASIS_RESOURCES_PATH="${CMAKE_SOURCE_DIR}/resources")

include(cmake/OasisFormatCode.cmake)
