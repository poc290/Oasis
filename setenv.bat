@echo off

set _MSVC_HOME="C:\Program Files (x86)\Microsoft Visual Studio\2017\Professional"

call %_MSVC_HOME%\Common7\Tools\VsDevCmd.bat -arch=amd64

set _QT_VERSION=5.12.12

set _QT_DIR=C:\Qt\%_QT_VERSION%
set _QT_TOOLS_DIR=%_QT_DIR%\Tools

set _QT_HOME=%_QT_DIR%\%_QT_VERSION%\msvc2017_64
set _QT_CREATOR_HOME=%_QT_TOOLS_DIR%\QtCreator
set _CMAKE_HOME="C:\Program Files\CMake"

set PATH=%_QT_HOME%\bin;%_QT_CREATOR_HOME%\bin;%_CMAKE_HOME%\bin;%PATH%

cmd /K