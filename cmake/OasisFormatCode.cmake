find_program(OASIS_CLANGFORMAT_EXE
             NAMES clang-format)

if(OASIS_CLANGFORMAT_EXE)
    message(STATUS "clang-format found: ${OASIS_CLANGFORMAT_EXE}")

    add_custom_target(format ALL
                      COMMAND ${OASIS_CLANGFORMAT_EXE} -i -style=file ${OASIS_SRC_FILES}
                      WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}"
                      COMMENT "Formatting code..."
                      VERBATIM)
endif()
