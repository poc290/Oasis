#
# Ce module recherche les fichiers d'en-tête et les fichiers binaires de la dépendance osgEarth.
# S'ils sont disponibles, alors les variables suivantes sont définies :
#  - OSGEARTH_FOUND,
#  - OSGEARTH_INCLUDE_DIR,
#  - OSGEARTH_LIBRARY.
#

# Le script `vcpkg.cmake` (cf. variable CMAKE_TOOLCHAIN_FILE) modifie la variable CMAKE_PREFIX_PATH
# en y ajoutant l'emplacement des paquets installés par vcpkg.
find_path(OSGEARTH_INCLUDE_DIR osgEarth/Version)

if(${CMAKE_BUILD_TYPE} STREQUAL "Release")
    find_library(OSGEARTH_LIBRARY osgEarth)
else()
    find_library(OSGEARTH_LIBRARY osgEarthd)
endif()

include(FindPackageHandleStandardArgs)

find_package_handle_standard_args(osgEarth FOUND_VAR OSGEARTH_FOUND
                                           REQUIRED_VARS OSGEARTH_LIBRARY OSGEARTH_INCLUDE_DIR)
