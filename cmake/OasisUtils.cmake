#
# Cette fonction copie les plugins OpenSceneGraph dans le répertoire où est produit l'exécutable (et
# à proximité de ce dernier).
#
function(oasis_copy_osg_plugins)
    if(OPENSCENEGRAPH_FOUND)
        if(${CMAKE_BUILD_TYPE} STREQUAL "Release")
            get_filename_component(_plugins_dir ${OSG_LIBRARY_RELEASE} DIRECTORY)
        else()
            get_filename_component(_plugins_dir ${OSG_LIBRARY_DEBUG} DIRECTORY)
        endif()
        string(APPEND _plugins_dir "/../plugins/osgPlugins-${OPENSCENEGRAPH_VERSION}")

        file(COPY ${_plugins_dir} DESTINATION ${CMAKE_BINARY_DIR})
    endif()
endfunction()
